<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>">
<head>
<?php print $head; ?>
<?php print $styles; ?>
<?php print $scripts; ?>
<title><?php print $head_title; ?></title>

<?php if (empty($sidebar_left)): ?>
  <link type="text/css" rel="stylesheet" media="all" href="<?php print base_path() . path_to_theme(); ?>/left.css" />
<?php endif; ?>

<?php if (empty($sidebar_left) && empty($sidebar_right)): ?>
  <link type="text/css" rel="stylesheet" media="all" href="<?php print base_path() . path_to_theme(); ?>/full.css" />
<?php endif; ?>

<!--  Free CSS Template | Designed by TemplateMo.com & http://topdrupalthemes.net  --> 
</head>
<body>
    <div id="templatemo_background_section_top">
    
    	<div class="templatemo_container">
          <div id="templatemo_logo_section">
            <h1><a href="<?php print check_url($base_path); ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
            <?php if($site_slogan): ?><h2><?php print $site_slogan; ?></h2><?php endif; ?>
          </div>
          <div id="templatemo_search_box">
     	    <?php print $feed_icons; ?>
            <?php print $search_box; ?>
          </div>
            
          <?php if (!empty($p_links)) : ?>
            <div id="templatemo_menu_section">
       		  		<ul>
       		  		  <?php print $p_links; ?>
              		</ul>               
			</div>
	      <?php endif; ?>
        </div>
    </div><!-- End Of Top Background -->
    <div id="templatemo_background_section_mid">
    	<div class="templatemo_container">
        <div id="templatemo_content_area">
        	<div id="templatemo_left_section">
              <?php if (!empty($breadcrumb)): ?><?php print $breadcrumb; ?><?php endif; ?>
		      <?php if (!empty($title) && empty($node)): ?><h2><?php print $title; ?></h2><?php endif; ?>
              <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
      		  <?php if (!empty($messages)): print $messages; endif; ?>
      		  <?php if (!empty($help)): print $help; endif; ?>
              <?php if ($is_front && !empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
           	  <?php print $content; ?>                    
                
            </div>
            <?php if(!empty($sidebar_left)): ?>
              <div id="templatemo_middle_section">  
                <?php print $sidebar_left; ?>                         
              </div>
            <?php endif; ?>
            
            <?php if(!empty($sidebar_right)): ?>
              <div id="templatemo_right_section">
                <?php print $sidebar_right; ?>                         
              </div>
             <?php endif; ?>
             
			<div class="cleaner_with_height"> </div>
			
		</div>
        <div id="templatemo_footer">
          <?php print $footer; ?>
          <?php print $footer_message; ?> <?php print $footer_msg; ?> | Designed by <a href="http://www.templatemo.com">TemplateMo.com</a> | <a href="http://topdrupalthemes.net">Drupal themes</a>
        </div>
        </div><!-- End Of container -->
    </div><!-- End Of Middle Background -->
   <?php print $closure; ?>
</body>
</html>
