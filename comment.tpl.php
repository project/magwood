<?php
?><div class="<?php print $classes; ?>">
  

    <?php if ($picture) { print $picture; } ?>
    
	<div class="commententry">
      <?php if ($comment->new): ?>
      	<span class="new"><?php print $new; ?></span>
      <?php endif; ?>

	  
        <div class="commentTitle">
          <?php print $title; ?>
        </div>

        <div class="submitted">
          <small><?php print $submitted; ?></small>
        </div>	
	
      <div class="content">
        <?php print $content ?>
        <?php if ($signature): ?>
          <div class="user-signature clear-block">
            <?php print $signature ?>
          </div>
        <?php endif; ?>
      </div>
    
     <div class="links">
       <?php print $links; ?>
     </div>

  
    </div>
  <div class="clear-block"> </div>
</div>
