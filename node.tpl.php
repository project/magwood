<?php
?><div class="templatemo_left_section_box" id="post-<?php print $node->nid; ?>">
  <div class="templatemo_left_section_box_top"><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></div>
  <div class="templatemo_left_section_box_body">
    
    <?php print $content; ?> 
 
    <?php if ($terms): ?>
      <div class="post"><?php print t('Tags'); ?>: <?php print $terms; ?></div> 
    <?php endif; ?>
    <div class="post"><?php print $posted_by; ?><?php if ($links): ?> | <?php  print $links; ?><?php endif; ?></div>
  </div>
</div>
